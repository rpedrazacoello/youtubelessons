package vanEmdeBoasTree;

import java.util.HashMap;

public class VanEmdeBoasTree {

    private HashMap<Long, VanEmdeBoasTree> clusters;
    private VanEmdeBoasTree summary;
    private Long min;
    private Long max;
    private long universeSize;
    private long numberOfClusters;


    public VanEmdeBoasTree(long universeSize) {
        this.universeSize = universeSize;

        if(universeSize > 2) {
            this.numberOfClusters = (long) Math.sqrt(universeSize);
            this.summary = new VanEmdeBoasTree(numberOfClusters);
            this.clusters = new HashMap<>();
        }
    }

    public void insert(long x){
        if (universeSize == 2){
            if(min == null){
                this.min = x;
                this.max = x;
            } else {
                if(x < min){
                    this.min = x;
                } else if (x > max){
                    this.max = x;
                }
            }
            return;
        }

        if(this.min == null){
            this.min = x;
            this.max = x;
            return;
        }

        if(x < min){
            long tmpMin = min;
            min = x;
            x = tmpMin;
        }

        if(x > max){
            max = x;
        }

        long clusterToInsert = high(x);
        long positionToInsert = low(x);

        if(!this.clusters.containsKey(clusterToInsert)){
            this.clusters.put(clusterToInsert, new VanEmdeBoasTree(numberOfClusters));
        }

        if(this.clusters.get(clusterToInsert).min == null){
            this.summary.insert(clusterToInsert);
        }
        this.clusters.get(clusterToInsert).insert(positionToInsert);
    }


    public long succesor(long x){
        if(universeSize == 2){
            if(this.max != null && x < this.max){
                return this.max;
            }

            return -1;
        }

        if(this.min == null){
            return -1;
        }

        if(x < this.min){
            return this.min;
        }

        long i = high(x);
        long j = low(x);

        VanEmdeBoasTree clusterOfX = null;
        if(this.clusters.containsKey(i)) {
            clusterOfX = this.clusters.get(i);
        }

        if(clusterOfX != null && clusterOfX.max != null && j < clusterOfX.max){
            j = clusterOfX.succesor(j);
        } else {
            i = this.summary.succesor(i);
            if(i == -1){
                return -1;
            }
            j = this.clusters.get(i).min;
        }

        return index(i, j);
    }


    public void delete(long x){
        if (universeSize ==2){
            if(this.min != null) {
                if (this.min.longValue() == this.max.longValue()) {
                    this.min = null;
                    this.max = null;
                } else {
                    if(x == this.min){
                        this.min = this.max;
                    } else {
                        this.max = this.min;
                    }
                }
            }
            return;
        }

        if(x == min) {
            /**
             * We have 2 possible option
             * 1. min is the last value of the whole tree
             * 2. there are other values in the tree
             */
            if (this.summary.min == null) {
                this.min = null;
                this.max = null;
                return;
            } else {
                long minIndex = this.summary.min;
                long minValue = this.clusters.get(minIndex).min;
                this.min = index(minIndex, minValue);
                x = min;
            }
        }

        long indexOfClusterToDeleteXFrom = high(x);
        VanEmdeBoasTree clusterToDeleteXFrom = clusters.get(indexOfClusterToDeleteXFrom);
        long positionToDelete = low(x);

        clusterToDeleteXFrom.delete(positionToDelete);
        if(clusterToDeleteXFrom.min == null){
            summary.delete(indexOfClusterToDeleteXFrom);
            clusters.remove(indexOfClusterToDeleteXFrom);
        }

        if(x == this.max){
            if(summary.max == null){
                this.max=this.min;
            } else {
                long indexMaxCluster = summary.max.longValue();
                VanEmdeBoasTree maxCluster = clusters.get(indexMaxCluster);
                this.max = index(indexMaxCluster, maxCluster.max);
            }
        }
    }

    private long high(long x){
        return x / numberOfClusters;
    }

    private long low(long x){
        return x % numberOfClusters;
    }

    private long index(long i, long j){
        if(i == -1 || j == -1){
            return -1;
        }
        return (i*numberOfClusters) + j;
    }
}
