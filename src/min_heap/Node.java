package min_heap;

public class Node {
    private int priorityValue;
    private Object value;
    private Node leftSon;
    private Node rightSon;
    private Node parent;

    public Node(int priorityValue, Object value) {
        this.priorityValue = priorityValue;
        this.value = value;
    }

    public int getPriorityValue() {
        return priorityValue;
    }

    public Object getValue() {
        return value;
    }

    public Node getLeftSon() {
        return leftSon;
    }

    public Node getRightSon() {
        return rightSon;
    }

    public Node getParent() {
        return parent;
    }

    public void setPriorityValue(int priorityValue) {
        this.priorityValue = priorityValue;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public void setLeftSon(Node leftSon) {
        this.leftSon = leftSon;
    }

    public void setRightSon(Node rightSon) {
        this.rightSon = rightSon;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public Node selectHigherPrioritySon(){
        if (this.rightSon == null) {
            return this.leftSon;
        }

        return this.leftSon.priorityValue <= this.rightSon.priorityValue ?
                this.leftSon : this.rightSon;
    }

    public boolean switchSon(Node oldSon, Node newSon){
        if(this.rightSon == oldSon){
            this.setRightSon(newSon);
            return true;
        } else if (this.leftSon == oldSon){
            this.setLeftSon(newSon);
            return true;
        }
        return false;
    }

    public boolean switchPlaceWithSon(Node son){
        Node parent = this.parent;
        Node leftSon = this.leftSon;
        Node rightSon = this.rightSon;

        Node sonLeftSon = son.leftSon;
        Node sonRightSon = son.rightSon;

        if(rightSon == son){
            if(this.parent != null){
                this.getParent().switchSon(this, son);
            }

            this.parent = son;
            son.rightSon = this;

            son.leftSon = leftSon;
            if(son.leftSon != null){
                son.leftSon.parent = son;
            }

            son.parent = parent;

            this.leftSon = sonLeftSon;
            if(this.leftSon != null) {
                this.leftSon.parent = this;
            }

            this.rightSon = sonRightSon;
            if(this.rightSon != null){
                this.rightSon.parent = this;
            }

            return true;

        } else if (leftSon == son){
            if(this.parent != null){
                this.getParent().switchSon(this, son);
            }

            this.parent = son;
            son.leftSon = this;

            son.rightSon = rightSon;
            if(son.rightSon != null){
                son.rightSon.parent = son;
            }


            son.parent = parent;

            this.rightSon = sonRightSon;
            if(this.rightSon != null){
                this.rightSon.parent = this;
            }


            this.leftSon = sonLeftSon;

            if(this.leftSon != null){
                this.leftSon.parent = this;
            }

            return true;
        }

        return false;
    }
}
