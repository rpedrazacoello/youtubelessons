package min_heap;

public class MinHeap {

    private Node root;
    private int count;

    public MinHeap() {
        this.count=0;
    }

    public void addNode(Node node){

        if(root == null){
            count = 1;
            root = node;
            return;
        }

        addToTheLastPosition(node);

        boolean heapify;
        do{
            heapify = maxHeapify(node.getParent());
            if(node.getParent() == null){
                this.root = node;
                break;
            }
        } while (heapify);
    }

    public Node removeRootNode(){
        if(root.getLeftSon() == null && root.getRightSon() == null){
            Node rootNode = root;
            this.root = null;
            count = 0;
            return rootNode;
        }

        Node lastNode = getNodeAtPosition(count);
        Node rootNode = this.root;
        switchNodes(lastNode, rootNode);
        removePointer(rootNode);

        boolean flag;
        do{
            flag = maxHeapify(lastNode);
            if(lastNode.getParent() == null){
                this.root = lastNode;
            } else if (lastNode.getParent().getParent() == null){
                this.root = lastNode.getParent();
            }
        }while (flag);

        count--;
        return rootNode;
    }

    public void increasePriority(Node node, int priorityValue){
        if(node.getPriorityValue() <= priorityValue){
            System.out.println("ONLY INCREASE");
            return;
        }

        node.setPriorityValue(priorityValue);
        boolean flag;
        do{
            flag = maxHeapify(node);
            if(flag){
                if(node.getParent() == null){
                    this.root = node;
                }
            }
        }while (flag);
    }

    private boolean maxHeapify(Node node){

        if(node.getParent() != null && node.getParent().getPriorityValue() >= node.getPriorityValue()){
            Node newParent = node.getParent().selectHigherPrioritySon();
            switchNodes(newParent, node.getParent());
            return true;
        }

        if((node.getLeftSon() == null || node.getPriorityValue() <= node.getLeftSon().getPriorityValue()) &&
                (node.getRightSon() == null || node.getPriorityValue() <= node.getRightSon().getPriorityValue())){
            return false;
        }

        Node newParent = node.selectHigherPrioritySon();
        switchNodes(node, newParent);
        return true;
    }

    private void addToTheLastPosition(Node node){
        count++;
        int positionOfParent = count / 2;
        Node parent = getNodeAtPosition(positionOfParent);

        if (parent.getLeftSon() == null) {
            parent.setLeftSon(node);
            node.setParent(parent);
        } else {
            parent.setRightSon(node);
            node.setParent(parent);
        }
    }

    private void removePointer(Node node){
        Node parent = node.getParent();

        if(node.getLeftSon() != null || node.getRightSon() != null){
            System.out.println("NOT POSSIBLE TO DELETE A NOT LEAF NODE");
            return;
        }

        if(parent != null){
            if(parent.getLeftSon() == node){
                parent.setLeftSon(null);
            } else {
                parent.setRightSon(null);
            }
            node.setParent(null);
        }
    }

    private Node getNodeAtPosition(int position){
        if (position == 0){
            return root;
        }

        int level = (int)(Math.log(position) / Math.log(2));
        int maxNumberOfNodesInLevel = (int) Math.pow(2, level);
        int numberOfNodesAboveLevel = maxNumberOfNodesInLevel - 1;
        int positionInLevel = position - numberOfNodesAboveLevel;

        int currentPosition = positionInLevel;
        int currentNumberOfLevels = level;
        Node currentNode = root;

        int positionBreaksTreeInHalf = maxNumberOfNodesInLevel / 2;
        do{

            if(currentNumberOfLevels > 0) {
                if (currentPosition <= positionBreaksTreeInHalf) {
                    currentNode = currentNode.getLeftSon();
                    currentNumberOfLevels--;
                } else {
                    currentNode = currentNode.getRightSon();
                    currentPosition = currentPosition - (int) Math.pow(2, --currentNumberOfLevels);
                }
                positionBreaksTreeInHalf = positionBreaksTreeInHalf/2;
            }
        } while(currentNumberOfLevels > 0);

        return currentNode;
    }

    private void switchNodes(Node node1, Node node2){
        if (node1.switchPlaceWithSon(node2)){
            return;
        } else if (node2.switchPlaceWithSon(node1)){
            return;
        }

        Node node1Parent = node1.getParent();
        Node node1LeftSon = node1.getLeftSon();
        Node node1RightSon = node1.getRightSon();

        Node node2Parent = node2.getParent();
        Node node2LeftSon = node2.getLeftSon();
        Node node2RightSon = node2.getRightSon();

        node1.setLeftSon(node2LeftSon);
        node1.setRightSon(node2RightSon);
        node1.setParent(node2Parent);

        node2.setParent(node1Parent);
        node2.setLeftSon(node1LeftSon);
        node2.setRightSon(node1RightSon);

        if (node1Parent != null) {
            if (node1Parent.getRightSon() == node1) {
                node1Parent.setRightSon(node2);
            } else {
                node1Parent.setLeftSon(node2);
            }
        }

        if (node2Parent != null) {
            if (node2Parent.getRightSon() == node2) {
                node2Parent.setRightSon(node1);
            } else {
                node2Parent.setLeftSon(node1);
            }
        }

        if(node1LeftSon != null){
            node1LeftSon.setParent(node2);
        }

        if(node1RightSon != null){
            node1RightSon.setParent(node2);
        }

        if(node2LeftSon != null){
            node2LeftSon.setParent(node1);
        }

        if(node2RightSon != null){
            node2RightSon.setParent(node1);
        }
    }

    public Node getRoot() {
        return root;
    }
}
