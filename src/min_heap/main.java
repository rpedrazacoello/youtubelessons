package min_heap;

public class main {

    public static void main (String [] args){
        Node node1 = new Node(10, "A");
        Node node2 = new Node(9, "B");
        Node node3 = new Node(8, "C");
        Node node4 = new Node(7, "D");
        Node node5 = new Node(6, "E");
        Node node6 = new Node(5, "F");
        Node node7 = new Node(4, "G");
        Node node8 = new Node(3, "H");
        Node node9 = new Node(2, "I");
        Node node10 = new Node(1, "J");

        MinHeap minHeap = new MinHeap();
        minHeap.addNode(node1);
        minHeap.addNode(node2);
        minHeap.addNode(node3);
        minHeap.addNode(node4);
        minHeap.addNode(node5);
        minHeap.addNode(node6);
        minHeap.addNode(node7);
        minHeap.addNode(node8);
        minHeap.addNode(node9);
        minHeap.addNode(node10);

        minHeap.increasePriority(node1, -node1.getPriorityValue());
        minHeap.increasePriority(node2, -node2.getPriorityValue());
        minHeap.increasePriority(node3, -node3.getPriorityValue());
        minHeap.increasePriority(node4, -node4.getPriorityValue());
        minHeap.increasePriority(node5, -node5.getPriorityValue());
        minHeap.increasePriority(node6, -node6.getPriorityValue());
        minHeap.increasePriority(node7, -node7.getPriorityValue());
        minHeap.increasePriority(node8, -node8.getPriorityValue());
        minHeap.increasePriority(node9, -node9.getPriorityValue());
        minHeap.increasePriority(node10, -node10.getPriorityValue());

        while (minHeap.getRoot() != null){
            System.out.println(minHeap.removeRootNode().getValue());
        }
    }
}
