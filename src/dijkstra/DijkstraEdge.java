package dijkstra;

public class DijkstraEdge {

    private int weight;
    private DijkstraNode reachNode;

    public DijkstraEdge(int weight, DijkstraNode reachNode) {
        this.weight = weight;
        this.reachNode = reachNode;
    }

    public int getWeight() {
        return weight;
    }

    public DijkstraNode getReachNode() {
        return reachNode;
    }
}
