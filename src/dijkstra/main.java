package dijkstra;

import jdk.swing.interop.SwingInterOpUtils;
import min_heap.MinHeap;

import java.util.Stack;

public class main {

    public static void main (String [] args){
        DijkstraNode nodeA = new DijkstraNode("A");
        DijkstraNode nodeB = new DijkstraNode("B");
        DijkstraNode nodeC = new DijkstraNode("C");
        DijkstraNode nodeD = new DijkstraNode("D");
        DijkstraNode nodeE = new DijkstraNode("E");
        DijkstraNode nodeF = new DijkstraNode("F");
        DijkstraNode nodeG = new DijkstraNode("G");

        nodeA.addEdge(nodeC, 4);
        nodeA.addEdge(nodeB, 1);

        nodeB.addEdge(nodeD, 1);

        nodeC.addEdge(nodeE, 3);

        nodeD.addEdge(nodeC, 1);
        nodeD.addEdge(nodeF, 1);

        nodeE.addEdge(nodeD, 2);
        nodeE.addEdge(nodeF, 2);

        nodeG.addEdge(nodeA, 3);

        MinHeap priorityQueue = new MinHeap();
        nodeA.setPriorityValue(0);
        priorityQueue.addNode(nodeA);

        dijkstraAlgorithm(priorityQueue);
        System.out.println("Node A");
        printShortestPath(nodeA);

        System.out.println("\nNode B");
        printShortestPath(nodeB);

        System.out.println("\nNode C");
        printShortestPath(nodeC);

        System.out.println("\nNode D");
        printShortestPath(nodeD);

        System.out.println("\nNode E");
        printShortestPath(nodeE);

        System.out.println("\nNode F");
        printShortestPath(nodeF);

        System.out.println("\nNode G");
        printShortestPath(nodeG);

    }

    private static void printShortestPath(DijkstraNode node){
        if(node.getPriorityValue() == Integer.MAX_VALUE){
            System.out.println("NO PATH TO NODE " + node.getValue());
            return;
        }

        System.out.println("Shortest Path Lenght: " +node.getPriorityValue());
        Stack<DijkstraNode> stack = new Stack<>();
        while(node.getDijkstraParent() != null){
            stack.add(node);
            node = node.getDijkstraParent();
        }
        stack.add(node);

        DijkstraNode dijkstraNode = stack.pop();
        while(!stack.empty()){
            System.out.print(dijkstraNode.getValue() + ", ");
            if(!stack.empty()){
                dijkstraNode = stack.pop();
            }
        }
        System.out.println(dijkstraNode.getValue());

    }

    private static void dijkstraAlgorithm(MinHeap priorityQueue){
        while (priorityQueue.getRoot() != null){
            DijkstraNode node = (DijkstraNode) priorityQueue.removeRootNode();

            for (DijkstraEdge edge: node.getOutEdges()) {
                DijkstraNode nodeEdge = edge.getReachNode();

                int oldDistance = nodeEdge.getPriorityValue();
                int newDistance = node.getPriorityValue() + edge.getWeight();

                if(newDistance < oldDistance){
                    if(oldDistance == Integer.MAX_VALUE){
                        nodeEdge.setPriorityValue(newDistance);
                        priorityQueue.addNode(nodeEdge);
                    } else {
                        priorityQueue.increasePriority(nodeEdge, newDistance);
                    }
                    nodeEdge.setDijkstraParent(node);
                }
            }
        }
    }
}
