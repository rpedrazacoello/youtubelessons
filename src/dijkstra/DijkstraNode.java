package dijkstra;

import min_heap.Node;

import java.util.ArrayList;

public class DijkstraNode extends Node {

    private ArrayList<DijkstraEdge> outEdges;
    private DijkstraNode dijkstraParent;

    public DijkstraNode(String value) {
        super(Integer.MAX_VALUE, value);
        outEdges = new ArrayList<>();
        dijkstraParent = null;
    }

    public void addEdge(DijkstraNode node, int weight){
        if(weight<0){
            System.out.println("NO NEGATIVE WEIGHTS");
            return;
        }

        DijkstraEdge edge = new DijkstraEdge(weight, node);
        outEdges.add(edge);
    }

    public ArrayList<DijkstraEdge> getOutEdges() {
        return outEdges;
    }

    public DijkstraNode getDijkstraParent() {
        return dijkstraParent;
    }

    public void setDijkstraParent(DijkstraNode dijkstraParent) {
        this.dijkstraParent = dijkstraParent;
    }
}
