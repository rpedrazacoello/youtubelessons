package dynamicProgramming;

import java.util.HashMap;

public class MaxSubarray {

    private static HashMap<Integer, Integer> memoization = new HashMap<>();
    private static HashMap<Integer, Integer> parent = new HashMap<>();

    public static void main(String[] args) {
        int[] array = {-1, 10, -1, 2};
        int max = Integer.MIN_VALUE; // -infinity
        int maxIndex = -1;
        for(int i=0; i<array.length; i++){
            int value = maxSubarray(i, array);
            if(value > max){
                max = value;
                maxIndex = i;
            }
        }
        System.out.println("" + max);

        System.out.println("MaxSubarray Starts at index: " + maxIndex);
        int currentIndex = maxIndex;
        while (parent.get(currentIndex) != null){
            currentIndex =parent.get(currentIndex);
        }
        System.out.println("MaxSubarray Ends at index: " + currentIndex);
    }


    private static int maxSubarray(int startIndex, int[] array){
        if(memoization.containsKey(startIndex)){
            return memoization.get(startIndex);
        }

        if(startIndex == array.length-1){
            memoization.put(startIndex, array[array.length-1]);
            parent.put(startIndex, null);
            return array[array.length-1];
        }

        int noRecursionValue = array[startIndex];
        int recursionValue = array[startIndex] + maxSubarray(startIndex + 1, array);

        int max = 0;
        if(recursionValue > noRecursionValue){
            max = recursionValue;
            parent.put(startIndex, startIndex+1);
        } else {
            max = noRecursionValue;
            parent.put(startIndex, null);
        }

        memoization.put(startIndex, max);
        return max;
    }
}
