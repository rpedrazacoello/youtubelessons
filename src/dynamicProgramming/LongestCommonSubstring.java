package dynamicProgramming;

import java.util.HashMap;
import java.util.Objects;

public class LongestCommonSubstring {

    private static HashMap<Integer, Integer> memoization = new HashMap<>();
    private static HashMap<Integer, Key> parent = new HashMap<>();

    public static void main(String[] args) {
        String s1 = "zxabcdezy";
        String s2 = "yzabcdezx";

        char[] s1Array = s1.toCharArray();
        char[] s2Array = s2.toCharArray();

        int max = Integer.MIN_VALUE;
        int iMax = -1;
        int jMax = -1;
        for(int i=0; i<s1Array.length; i++){
            for(int j=0; j<s2Array.length; j++){
                int lenght = longestSubtstring(s1Array, s2Array, i, j);
                if(lenght>max){
                    max=lenght;
                    iMax = i;
                    jMax = j;
                }
            }
        }

        System.out.println("Longest substring lenght:" + max);
//        int currentI = iMax;
//        int currentJ = jMax;
//        Key currentKey = new Key(currentI, currentJ);
//
//        System.out.print("Longest substring: ");
//        while(parent.get(currentKey.hashCode()) != null){
//            System.out.print(s1Array[currentI]);
//            currentKey = parent.get(currentKey.hashCode());
//            currentI = currentKey.i;
//        }

        System.out.print("Longest substring: ");
        for(int i=0; i<max; i++){
            System.out.print(s1Array[iMax+i]);
        }
    }

    private static int longestSubtstring(char[] s1Array, char[] s2Array, int i, int j){
        int key = new Key(i,j).hashCode();

        if(memoization.containsKey(key)){
            return memoization.get(key);
        }

        if(i == s1Array.length || j == s2Array.length){
            return 0;
        }

        int lenght;
        if(s1Array[i] == s2Array[j]){
            lenght = 1 + longestSubtstring(s1Array, s2Array, i+1, j+1);
            parent.put(key, new Key(i+1, j+1));
        } else {
            lenght = 0;
            parent.put(key, null);
        }

        memoization.put(key, lenght);
        return lenght;
    }

    private static class Key{
        private int i;
        private int j;

        public Key(int i, int j) {
            this.i = i;
            this.j = j;
        }

        @Override
        public int hashCode() {
            return Objects.hash(i, j);
        }
    }
}
