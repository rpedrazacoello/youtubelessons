package dynamicProgramming;

import java.util.HashMap;

public class TextJustification {

    private static HashMap <Integer, Integer> memoization = new HashMap<>();
    private static HashMap <Integer, Integer> parents = new HashMap<>();
    private static HashMap <Integer, Integer> extraSpaces = new HashMap<>();
    private static int rowLenght = 60;

    public static void main(String[] args) {
        String text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus scelerisque, leo non egestas ultrices, nulla lectus iaculis purus, consequat lacinia orci sem eu leo. Suspendisse in nulla eget dui aliquam placerat. Donec eget lacus quis dui fringilla pharetra id quis lectus. Morbi eros libero, rutrum vel tempor vel, auctor sed neque. Pellentesque accumsan ut risus eu consectetur. Quisque a dui hendrerit, fermentum augue id, tincidunt ipsum. Morbi hendrerit hendrerit lorem, ut mattis felis venenatis eget. Praesent nulla eros, tincidunt eget dapibus id, semper sed sapien. Proin quis ex tincidunt, luctus nulla eu, viverra quam. Fusce iaculis tempus elit ac sodales. Aliquam ullamcorper eleifend est, eu volutpat sem ultricies ut. Sed congue tincidunt urna, non iaculis nisl pharetra sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus scelerisque, leo non egestas ultrices, nulla lectus iaculis purus, consequat lacinia orci sem eu leo. Suspendisse in nulla eget dui aliquam placerat. Donec eget lacus quis dui fringilla pharetra id quis lectus. Morbi eros libero, rutrum vel tempor vel, auctor sed neque. Pellentesque accumsan ut risus eu consectetur. Quisque a dui hendrerit, fermentum augue id, tincidunt ipsum. Morbi hendrerit hendrerit lorem, ut mattis felis venenatis eget. Praesent nulla eros, tincidunt eget dapibus id, semper sed sapien. Proin quis ex tincidunt, luctus nulla eu, viverra quam. Fusce iaculis tempus elit ac sodales. Aliquam ullamcorper eleifend est, eu volutpat sem ultricies ut. Sed congue tincidunt urna, non iaculis nisl pharetra sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus scelerisque, leo non egestas ultrices, nulla lectus iaculis purus, consequat lacinia orci sem eu leo. Suspendisse in nulla eget dui aliquam placerat. Donec eget lacus quis dui fringilla pharetra id quis lectus. Morbi eros libero, rutrum vel tempor vel, auctor sed neque. Pellentesque accumsan ut risus eu consectetur. Quisque a dui hendrerit, fermentum augue id, tincidunt ipsum. Morbi hendrerit hendrerit lorem, ut mattis felis venenatis eget. Praesent nulla eros, tincidunt eget dapibus id, semper sed sapien. Proin quis ex tincidunt, luctus nulla eu, viverra quam. Fusce iaculis tempus elit ac sodales. Aliquam ullamcorper eleifend est, eu volutpat sem ultricies ut. Sed congue tincidunt urna, non iaculis nisl pharetra sit amet.";
        String[] arrayText = text.split(" ");
        System.out.println(justifyText(0, arrayText));
        printJustifiedText(arrayText);
    }

    private static int justifyText(int i, String[] textArray){
        if(memoization.containsKey(i)){
            return memoization.get(i);
        }

        if(i == textArray.length-1){
            memoization.put(i, 0);
            parents.put(i, null);
            extraSpaces.put(i, 0);
            return 0;
        }

        int count = textArray[i].length();
        int spaces = 0;

        int minI = 0;
        int minWeight = Integer.MAX_VALUE; // == infinity
        int minExtraSpaces = 0;
        int currentI = i+1;

        do{

            int weight = weight(rowLenght - count - spaces);
            int newRowWeight = justifyText(currentI, textArray);

            if(weight + newRowWeight < minWeight){
                minI = currentI;
                minWeight = weight + newRowWeight;
                minExtraSpaces = rowLenght - count - spaces;
            }


            count += textArray[currentI].length();
            currentI++;
            spaces++;
        }while(rowLenght - count - spaces >= 0 && currentI <= textArray.length-1);

        memoization.put(i, minWeight);
        parents.put(i, minI);
        extraSpaces.put(i, minExtraSpaces);
        return minWeight;
    }

    private static int weight(int numberExtraSpaces){
        return numberExtraSpaces^3;
    }

    private static void printJustifiedText(String[] textArray){
        System.out.println("\n");
        int currentI = 0;

        while(parents.get(currentI)!=null){
            int nextRowStart = parents.get(currentI);
            int numberOfWordsInRow = nextRowStart - currentI;
            int numberOfExtraSpaces = extraSpaces.get(currentI);
            int extraSpacesPerWord = (int) Math.ceil((double)numberOfExtraSpaces/(double)(numberOfWordsInRow-1));

            for(int i=currentI; i<nextRowStart; i++){
                System.out.print(textArray[i]);
                if(i != nextRowStart-1) {
                    System.out.print(" ");
                    for (int j = 0; j < extraSpacesPerWord; j++) {
                        if (numberOfExtraSpaces > 0) {
                            System.out.print(" ");
                            numberOfExtraSpaces--;
                        }
                    }
                }
            }
            System.out.println();
            currentI = nextRowStart;
        }

        for(int i=currentI; currentI<textArray.length; currentI++){
            System.out.print(textArray[i]);
            if(i != textArray.length-1) {
                System.out.print(" ");
            }
        }
        System.out.println("\n");
    }
}
