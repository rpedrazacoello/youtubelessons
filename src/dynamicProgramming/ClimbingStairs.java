package dynamicProgramming;

import java.util.HashMap;

public class ClimbingStairs {

    private static HashMap<Integer, Integer> memoization = new HashMap<>();

    public static void main(String[] args) {
        int n = 5;
        int [] steps = {1, 5, 2};
        System.out.println(climbStairs(n, steps));
    }


    private static int climbStairs (int stepsLeft, int[] steps){
        if(stepsLeft == 0){
            return 1;
        }

        if(stepsLeft < 0){
            return 0;
        }

        if(memoization.containsKey(stepsLeft)){
            return memoization.get(stepsLeft);
        }

        int count = 0;
        for(int i=0; i<steps.length; i++) {
            count += climbStairs(stepsLeft - steps[i], steps);
        }

        memoization.put(stepsLeft, count);
        return (count);
    }
}
