package depth_first_search;

import java.util.HashMap;
import java.util.Stack;

public class main {

    private static Stack<Node> topologicalSort = new Stack();
    private static HashMap<String, Node> unvisited = new HashMap();
    private static boolean cycleFound = false;

    private static HashMap<String, Node> parent = new HashMap<>();

    public static void main (String [] args){
        Node nodeA = new Node("A");
        Node nodeB = new Node("B");
        Node nodeC = new Node("C");
        Node nodeD = new Node("D");
        Node nodeE = new Node("E");
        Node nodeF = new Node("F");
        Node nodeG = new Node("G");
        Node nodeH = new Node("H");

        nodeA.addNeighbor(nodeB);

        nodeB.addNeighbor(nodeC);
        nodeB.addNeighbor(nodeD);

        nodeC.addNeighbor(nodeE);
        nodeC.addNeighbor(nodeF);

        nodeD.addNeighbor(nodeG);

        nodeG.addNeighbor(nodeH);

        //nodeH.addNeighbor(nodeB);

        unvisited.put(nodeA.getId(), nodeA);
        unvisited.put(nodeB.getId(), nodeB);
        unvisited.put(nodeC.getId(), nodeC);
        unvisited.put(nodeD.getId(), nodeD);
        unvisited.put(nodeE.getId(), nodeE);
        unvisited.put(nodeF.getId(), nodeF);
        unvisited.put(nodeG.getId(), nodeG);
        unvisited.put(nodeH.getId(), nodeH);

        int subGraphId = 1;

        while (!unvisited.isEmpty()){
            Node startNode = (Node) unvisited.values().toArray()[0];
            visitNeighbors(startNode, subGraphId);
            subGraphId++;
        }

        if(!cycleFound) {
            printTopologicalSort();
        }
    }

    private static void visitNeighbors(Node node, int subGraphId){
        node.setSubgraphId(subGraphId);
        for (Node neighbor: node.getNeighbors()) {
            if(node.getSubgraphId() == neighbor.getSubgraphId()){
                System.out.println("CYCLE FOUND!");
                cycleFound = true;

                Node tmpNode = node;

                while (tmpNode != neighbor){
                    System.out.print(tmpNode.getId() +", ");
                    tmpNode = parent.get(tmpNode.getId());
                }

                System.out.print(neighbor.getId());
                return;
            }

            if(!neighbor.isVisited()){
                parent.put(neighbor.getId(), node);
                visitNeighbors(neighbor, subGraphId);
            }
        }

        topologicalSort.add(node);
        node.setVisited(true);
        unvisited.remove(node.getId());
    }


    private static void printTopologicalSort(){
        while (!topologicalSort.empty()){
            System.out.print(topologicalSort.pop().getId());
            if(!topologicalSort.empty()){
                System.out.print(", ");
            }
        }
    }
}
