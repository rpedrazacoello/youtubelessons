package depth_first_search;

import java.util.ArrayList;

public class Node {
    private String id;
    private ArrayList<Node> neighbors;
    private boolean visited = false;

    private int subgraphId = 0;

    public Node(String id) {
        this.id = id;
        this.neighbors = new ArrayList<>();
    }

    public void addNeighbor(Node neighbor){
        neighbors.add(neighbor);
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public String getId() {
        return id;
    }

    public ArrayList<Node> getNeighbors() {
        return neighbors;
    }

    public int getSubgraphId() {
        return subgraphId;
    }

    public void setSubgraphId(int subgraphId) {
        this.subgraphId = subgraphId;
    }
}
