package breadht_first_search;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class main {

    private static HashMap<String, Node> parent = new HashMap();

    public static void main (String [] args){
        Node nodeA = new Node("A");
        Node nodeB = new Node("B");
        Node nodeC = new Node("C");
        Node nodeD = new Node("D");
        Node nodeE = new Node("E");
        Node nodeF = new Node("F");
        Node nodeG = new Node("G");
        Node nodeH = new Node("H");

        nodeA.addNeighbor(nodeB);
        nodeA.addNeighbor(nodeC);

        nodeB.addNeighbor(nodeC);

        nodeC.addNeighbor(nodeD);

        nodeD.addNeighbor(nodeE);
        nodeD.addNeighbor(nodeF);

        nodeE.addNeighbor(nodeG);
        nodeF.addNeighbor(nodeF);

        breadthFirstSearch(nodeA);
        findShortesPath(nodeH);
    }

    private static void breadthFirstSearch(Node startNode){
        Queue<Node> currentNodes = new LinkedList<>();
        currentNodes.add(startNode);
        startNode.setLevel(0);
        parent.put(startNode.getId(), startNode);

        while(!currentNodes.isEmpty()){
            Node currentNode = currentNodes.remove();

            for(Node neighbor: currentNode.getNeighbors()){
                if (neighbor.getLevel() == null){
                    neighbor.setLevel(currentNode.getLevel() + 1);
                    currentNodes.add(neighbor);
                    parent.put(neighbor.getId(), currentNode);
                }
            }
        }

    }

    private static void findShortesPath(Node searchNode){
        if(searchNode.getLevel() == null){
            System.out.println("NO POSSIBLE PATH");
            return;
        }

        Node currentNode = searchNode;

        while(parent.get(currentNode.getId()) != currentNode){
            System.out.print(currentNode.getId() +", ");
            currentNode = parent.get(currentNode.getId());
        }
        System.out.println(currentNode.getId());
        System.out.println("Path length = " + searchNode.getLevel());
    }
}
