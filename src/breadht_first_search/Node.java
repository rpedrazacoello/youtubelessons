package breadht_first_search;

import java.util.ArrayList;

public class Node {
    private ArrayList<Node> neighbors;
    private String id;
    private Integer level;

    public Node(String id) {
        this.id = id;
        this.neighbors = new ArrayList<>();
    }

    public void addNeighbor(Node neighbor){
        this.neighbors.add(neighbor);
        neighbor.neighbors.add(this);
    }

    public ArrayList<Node> getNeighbors() {
        return neighbors;
    }

    public String getId(){
        return this.id;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
}
